# python_page_object
Тестовый проект с ипользованием page_object

Подготовка

1. Склонировать проект
git clone https://github.com/mefrum/stepik_page_object.git

2. Перейти в папку с проектом

3. Создать виртуальное окружение python -m venv env

4. Активировать виртуальное окружение 
win - env\Scripts\activate
linux - source ./env/bin/activate

5. Установить зависимости 
pip install -r requirements.txt

6. Установить и настроить вебдрайверы для Chrome и Firefox
          

Запуск тестов 
pytest -v --tb=line --language=en -m example

